# nr_backup

## 1.简介

nr_backup是一份基于restic的自动化批量备份工具，目的是解决个人数据的批量自动化备份。

使用场景：
- 个人NAS关键数据备份。如果NAS使用RAID1提高数据可靠性，不仅降低了磁盘IO速度，还浪费磁盘空间。使用该工具将主磁盘上的关键数据定时备份到副盘上，可以实现RAID类似的效果，但不降速，也不占空间。

## 2. 安装

适用平台
- unbuntu

该工具依赖
- python version > 3.7
- restic （安装方法参见 https://restic.readthedocs.io/en/latest/）

## 3. 快速指南

### 3.1 配置方法

```json
{
	"example": {
		"repo_type": "local",
		"floder_need_to_backup": "需要备份文件夹目录（绝对路径）",
		"exclude_setting_file": "无需备份的文件过滤正则表达式文件，类似于gitignore，不填表示，备份所有文件",
		"include_setting_file": "类似exclude_setting_file",
		"repo_path": "备份仓库库路径（绝对路径）",
		"repo_password": "仓库密码",
		"keep_policy": "设置遗忘策略，如"--keep_last 5", 保留最近5次，其余策略参考restic官方文档",
		"scheduler_policy": "",
		"pre_exec": "",
		"init_status": "备份仓库的初始化状态<true|false>，若为false，表示未初始化，会自动初始化"
	},
	"xxxx": {
		.......
	}
}
```

### 3.2 使用方法
```sh
# init
python3 nr_backup.py init <your cfg.json path>

# backup
python3 nr_backup.py backup <your cfg.json path>
```

如果需要备份root创建的文件，需要给restic提权
```sh
sudo setcap cap_dac_read_search=+ep /usr/bin/restic
```

目前未实现内置的周期备份，使用cron周期执行backup，以实现备份
```sh
# 弹窗填入执行命令配置
crontab -e
# 配置每天4:00执行一次备份
0 4 * * * python3 /home/xx/nr_backup/nr_backup.py backup /home/xx/nr_backup/backup_config.json >> /home/xx/nr_backup/nr_backup.log
# 查看是否开启定时任务
crontab -l
```

### 3.3 使用示例

TODO

## 4. TODO

-[] 支持远程备份
-[] 实现内部定时备份

