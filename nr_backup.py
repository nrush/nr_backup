from getopt import getopt
import json
from mimetypes import init
from ssl import DER_cert_to_PEM_cert
import time
import os
import subprocess
import re
import sys
import getopt

default_password="nr_backup"

global_log_path=""

def log_out(str, level = "I", log_out_path=global_log_path):
	time_str = time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(time.time()))
	print("{} [{}] {}".format(time_str, level, str))

def load_cfg(path):
	with open(path, "r") as f:
			raw_data = json.load(f)
	return raw_data

def save_cfg(path, cfgs):
	with open(path, "w+") as f:
			json.dump(cfgs, f, indent = '\t')

def exec_cmd(cmd, env=None, log_out_path=""):
	log_out("run \"{}\"".format(cmd))
	out =  subprocess.run(cmd,	capture_output=True, env=env, shell=True)
	for ln in out.stdout.decode('ascii').split('\n') :
			if ln != "":
				log_out(ln)
	
	for ln in out.stderr.decode('ascii').split('\n') :
			if ln != "":
				log_out(ln, "E")
	return out

def is_parent_path_exist(path):
	abspath = path.rstrip('/')
	abspath = os.path.dirname(abspath)
	ret = os.path.exists(abspath)
	return ret


def __init_repo(cfg):
	ret = False
	init_status = cfg["init_status"]
	if init_status == "false":
		repo_path = cfg["repo_path"]
		password =  cfg["repo_password"]
		repo_type = cfg["repo_type"]
		log_out("The repo {} is not initialized, init ....".format(repo_path))
		if password == "":
			password = default_password

		os.environ["RESTIC_PASSWORD"] = password
		if repo_type == "local":

			if is_parent_path_exist(repo_path) == False:
				log_out("The local path {} does not exist!".format(os.path.dirname(repo_path.rstrip('/'))) )
				return cfg, ret

			out = exec_cmd("restic -r {} init".format(repo_path), env=os.environ)
			if out.returncode == 0:
				log_out("Initialize {} sucessfully !".format(repo_path))
				cfg["init_status"] = "true"
				ret = True
			else:
				err_str =  out.stderr.decode('ascii')
				re_ans = re.findall("(config file already exists)", err_str)
				if len(re_ans):

					# check the password
					out1 =  exec_cmd("restic -r {} snapshots".format(repo_path), env=os.environ)
					if out1.returncode != 0:
						log_out("The repo {} already exists, but the password is wrong".format(repo_path), "E")
						return cfg, False

					ret = True
					cfg["init_status"] = "true"
					log_out("Initialize {} sucessfully !".format(repo_path))
				else:
					ret = False
					log_out("Failed to Initialize {}, ret {}".format(repo_path, out.returncode))
	return cfg, ret

def init_repo(cfg_path, cfg_name):
	cfgs = load_cfg(cfg_path)
	cfg = cfgs[cfg_name]
	cfg, ret = __init_repo(cfg)
	cfgs[cfg_name] = cfg
	save_cfg(cfg_path, cfgs)
	return ret

def __backup(cfg, tag="auto"):
	repo_path = cfg["repo_path"]
	floder = cfg["floder_need_to_backup"]
	password =  cfg["repo_password"]
	repo_type = cfg["repo_type"]
	exclude_setting_file = cfg["exclude_setting_file"]
	os.environ["RESTIC_PASSWORD"] = password

	if os.path.exists(floder) == False:
		return False

	if repo_type == "local":

		if os.path.exists(repo_path) == False:
				return False

		cmd="restic -r {} backup --tag {} {}".format(repo_path, tag, floder)
		if exclude_setting_file != "":
			cmd = cmd + " --exclude-file={}".format(exclude_setting_file)
		out = exec_cmd(cmd, env=os.environ)

	if out.returncode == 0:
		return True
	else :
		return False

def __forget(cfg):
	repo_path = cfg["repo_path"]
	floder = cfg["floder_need_to_backup"]
	password =  cfg["repo_password"]
	keep_policy = cfg["keep_policy"]
	repo_type = cfg["repo_type"]

	os.environ["RESTIC_PASSWORD"] = password

	if keep_policy != "":
		if repo_type == "local":
			out = exec_cmd("restic -r {} forget {} --prune".format(repo_path, keep_policy), env=os.environ)
	
			if out.returncode == 0:
				return True
			else :
				return False
	else :
		return True

def backup(cfg_path):
	cfgs = load_cfg(cfg_path)

	for cfg_str in cfgs:
		cfg = cfgs[cfg_str]
		init_status = cfg["init_status"]

		if init_status == "false":
			ret = init_repo(cfg_path, cfg_str)
			if ret == False:
				return False

		ret = __backup(cfg)
		# if backup failed, we just try to init the repo again
		if ret == False:
			cfg["init_status"] = "false"
			cfgs[cfg_str] = cfg
			continue
		__forget(cfg)
	
	save_cfg(cfg_path, cfgs)

def init(cfg_path):
	cfgs = load_cfg(cfg_path)
	for cfg_name in cfgs:
		cfg = cfgs[cfg_name]
		cfg, ret = __init_repo(cfg)
		cfgs[cfg_name] = cfg
	save_cfg(cfg_path, cfgs)

def parse_args():
	def print_usage():
		print("Useage:")
		print("python nr_backup.py <operation> <cfg_path>")
		print("operation:")
		print("     init: init all repo.")
		print("   backup: backup the floders configed in json.")

	if sys.argv[1] not in ["init", "backup"]:
		print_usage()
		return

	if sys.argv[1] == "init":
		cfg_path=sys.argv[2]
		init(cfg_path)
	elif sys.argv[1] == "backup":
		cfg_path=sys.argv[2]
		backup(cfg_path)

if __name__ == "__main__":
	parse_args()
