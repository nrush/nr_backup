#!/bin/sh

date=`date +%Y-%m-%d-%H:%M:%S`

src=$1
config=$src/nr_backup_cfg.json
log=$src/nr_backup.log
data=$src/nr_backup_dat.json
version="1.0.0"

dst=`jq ".target_dir" $config -r`
preload=`jq ".preload" $config -r`
sync_per_seconds=`jq .sync_per_seconds $config -r`

cur=`date +%s`
if [ -e $data ]; then
	total_sync_times=`jq .total_sync_times $data -r`
	failed_sync_times=`jq .failed_sync_times $data -r`
	last_sync_time=`jq .last_sync_time_in_seconds $data -r`
else
	total_sync_times=0
	last_sync_time=0
	failed_sync_times=0
fi

logout()
{
	echo "[`date +%Y-%m-%d-%H:%M:%S`]  |  $1" | tee -a $log
}

do_update_data()
{
	echo '{"total_sync_times":"'$total_sync_times'","last_sync_time_in_seconds":"'$last_sync_time'","failed_sync_times":"'$failed_sync_times'"}' | jq . > $data
}

do_rsync()
{
	start_time=`date +%s`
	logout "nr_backup.sh version: $version"
	logout "exec perload..."
	logout `$preload`
	logout "hostname: $HOSTNAME, user: $USER, source_dir: $src, tagert_dir: $dst"
	logout "start to rsync..."
	logout "------------------------------------------------"
	rsync -av --delete $src $dst | tee -a $log
	logout "------------------------------------------------"
	if [ $? -ne 0 ]; then
		logout "rsync failed!"
		let failed_sync_times++
	fi
	last_sync_time=`date +%s`
	let total_sync_times++
	logout "last sync time: `date +%s-%Z`, total_sync_times: $total_sync_times, failed_sync_times: $failed_sync_times"
	logout "update $data"
	do_update_data
	end_time=`date +%s`
	logout "finish sync. cost $[ $end_time - $start_time ]s"
	logout `rsync -av --delete $src $dst`
}

if [ $[ $cur - $last_sync_time ] -ge $sync_per_seconds ]; then
	do_rsync
fi


